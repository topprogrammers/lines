#include "iostream";
#include "iomanip";
using namespace std;

const int N = 9;

void PrintMatrix(int[][N]);
void GenerateRandomBalls();

int main()
{
	int Map[N][N] = { { 0 }, { 0 }, { 0 }, { 0 }, { 0 }, { 0 }, { 0 }, { 0 }, { 0 } };

	system("pause");
	return 0;
}

void PrintMatrix(int Matrix[N][N])
{
	cout.setf(ios::fixed);

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			cout << setfill(' ') << setw(2) << Matrix[i][j];
		}
		cout << endl;
	}
}